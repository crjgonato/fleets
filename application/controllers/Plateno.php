<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Plateno extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Plate No';

		$this->load->model('model_plateno');
		//for creating group
		$this->load->model('model_users');
	
   }

   /* 
	* It only redirects to the manage plateno page
	* It passes the total plateno information
	into the frontend.
	*/
	public function index()
	{
		if(!in_array('viewPlateno', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$this->render_template('plateno/index', $this->data);
	}

	public function fetchPlatenoDataById($id) 
	{
		if($id) {
			$data = $this->model_plateno->getPlatenoData($id);
			echo json_encode($data);
		}

		return false;
   }
	//Plate numbers list
	public function fetchPlatenoData()
	{
		$result = array('data' => array());

		$data = $this->model_plateno->getPlatenoData();

		foreach ($data as $key => $value) {

			//$trucks_data = $this->model_trucks->getTrucksData($value['trucks_id']);
			$buttons = '';

			if(in_array('updatePlateno', $this->permission)) {
				$buttons .= '<button type="button" class="btn btn-default btn-sm" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
			}

			if(in_array('deletePlateno', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
			$status = ($value['status'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

			$result['data'][$key] = array(
				$value['name'],
				// $value['gasoline'],
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}
	public function create()
	{
		if(!in_array('createPlateno', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		
		$response = array();

		$this->form_validation->set_rules('plateno', 'Plate Number', 'trim|required');
		$this->form_validation->set_rules('status', 'Active', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'name' => $this->input->post('plateno'),
        		'status' => $this->input->post('status'),	
        	);

        	$create = $this->model_plateno->create($data);
        	if($create == true) {
        		$response['success'] = true;
				  $response['messages'] = 'Succesfully created';
				  //redirect('trucks/', 'refresh');
        	}
        	else {
        		$response['success'] = false;
				  $response['messages'] = 'Error while creating new truck';
				  //redirect('trucks/create', 'refresh');			
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}
	public function update($id)
	{

		if(!in_array('updatePlateno', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		if($id) {
         $this->form_validation->set_rules('edit_plateno', 'Plate Number', 'trim|required');
         
			$this->form_validation->set_rules('edit_status', 'Active', 'trim|required');

			// $this->form_validation->set_rules('edit_gasoline', 'Gasoline', 'trim|required');

			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
					
					'name' => $this->input->post('edit_plateno'),
					// 'gasoline' => $this->input->post('edit_gasoline'),
	        		'status' => $this->input->post('edit_status'),	
	        	);

	        	$update = $this->model_plateno->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error while updating truck';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}
	public function remove()
	{
		if(!in_array('deletePlateno', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$plateno_id = $this->input->post('plateno_id');

      $response = array();
      //console.log($response);
		if($plateno_id) {
			$delete = $this->model_plateno->remove($plateno_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
				//redirect('trucks/', 'refresh');
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error while removing customer";
				//redirect('trucks/', 'refresh');
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
			//redirect('trucks/', 'refresh');
		}

		echo json_encode($response);
	}
}