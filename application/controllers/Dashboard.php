<?php 

class Dashboard extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Dashboard';

		$this->load->model('model_trucks');
		$this->load->model('model_destinations');
		$this->load->model('model_plateno');
		$this->load->model('model_masterlist');

	}

	/* 
	* It only redirects to the manage category page
	* It passes the total product, total paid orders, total users, and total stores information
	into the frontend.
	*/

	public function index()
	{
		$this->data['total_trucks'] = $this->model_trucks->countTotalTrucks();
		$this->data['total_destinations'] = $this->model_destinations->countTotalDestinations();
		$this->data['total_plateno'] = $this->model_plateno->countTotalPlateno();
		$this->data['total_masterlist'] = $this->model_masterlist->countTotalMasterlist();
		//model master list for dashboard
		$this->data['model_masterlist'] = $this->model_masterlist->getMasterlistData();
		
		$user_id = $this->session->userdata('id');
		$is_admin = ($user_id == 1 || $user_id == 6 ) ? true :false;

		$this->data['is_admin'] = $is_admin;
		$this->render_template('dashboard', $this->data);
	}
}