<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Trucks extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Trucks';

	 	$this->load->model('model_trucks');
		// $this->load->model('model_destinations');
	
	}

	/* 
	* It only redirects to the manage trucks page
	* It passes the total trucks information
	into the frontend.
	*/
	public function index()
	{
		// $this->data['total_trucks'] = $this->model_trucks->countTotalTrucks();
		// $this->data['total_destinations'] = $this->model_destinations->countTotalDestinations();

		if(!in_array('viewTrucks', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$this->render_template('trucks/index', $this->data);
	}

	/*
	* It checks if it gets the trucks id and retreives
	* the trucks information from the trucks model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchTrucksDataById($id) 
	{
		if($id) {
			$data = $this->model_trucks->getTrucksData($id);
			echo json_encode($data);
		}

		return false;
   }
	//Truck list
	public function fetchTrucksData()
	{
		$result = array('data' => array());

		$data = $this->model_trucks->getTrucksData();

		foreach ($data as $key => $value) {

			//$trucks_data = $this->model_trucks->getTrucksData($value['trucks_id']);
			$buttons = '';

			if(in_array('updateTrucks', $this->permission)) {
				$buttons .= '<button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-default btn-sm" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
			}

			if(in_array('deleteTrucks', $this->permission)) {
				$buttons .= ' <button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-default btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
			$status = ($value['status'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

			$result['data'][$key] = array(
				$value['name'],
				// $value['date_added'],
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}
	
	//


	/* */
	public function fetchTrucksDataDashboard()
	{
		$result = array('data' => array());

		$data = $this->model_trucks->getTrucksData();

		foreach ($data as $key => $value) {

			//$routes_data = $this->model_routes->getRoutesData($value['routes_id']);
			// $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
			
			$result['data'][$key] = array(
				$value['name'],
				// $value['status']
				// $value['expenses'],
				// $status
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* Its checks the truck form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		if(!in_array('createTrucks', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		
		$response = array();

		$this->form_validation->set_rules('trucks_name', 'Trucks name', 'trim|required');
		$this->form_validation->set_rules('status', 'Active', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'name' => $this->input->post('trucks_name'),
        		'status' => $this->input->post('status'),	
        	);

        	$create = $this->model_trucks->create($data);
        	if($create == true) {
        		$response['success'] = true;
				  $response['messages'] = 'Succesfully created';
				  //redirect('trucks/', 'refresh');
        	}
        	else {
        		$response['success'] = false;
				  $response['messages'] = 'Error while creating new truck';
				  //redirect('trucks/create', 'refresh');			
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}

	/*
	* Its checks the trucks form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function update($id)
	{

		if(!in_array('updateTrucks', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		if($id) {
         $this->form_validation->set_rules('edit_trucks_name', 'Truck Name', 'trim|required');
         
         $this->form_validation->set_rules('edit_status', 'Active', 'trim|required');

			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
					
					'name' => $this->input->post('edit_trucks_name'),
               
	        		'status' => $this->input->post('edit_status'),	
	        	);

	        	$update = $this->model_trucks->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error while updating truck';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}

	//Remove()
   /*
	* It removes the customer information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deleteTrucks', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$trucks_id = $this->input->post('trucks_id');

      $response = array();
      //console.log($response);
		if($trucks_id) {
			$delete = $this->model_trucks->remove($trucks_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
				//redirect('trucks/', 'refresh');
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error while removing customer";
				//redirect('trucks/', 'refresh');
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
			//redirect('trucks/', 'refresh');
		}

		echo json_encode($response);
	}
}