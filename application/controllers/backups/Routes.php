<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes extends Admin_Controller 
{
   public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Routes';

		$this->load->model('model_routes');
   }
   

   /* 
	* It only redirects to the manage route page
	*/
	public function index()
	{
		if(!in_array('viewRoutes', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$this->render_template('routes/index', $this->data);	
   }	
   
   /*
	* It checks if it gets the routes id and retreives
	* the routes information from the routes model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchRoutesDataById($id) 
	{
		if($id) {
			$data = $this->model_routes->getRoutesData($id);
			echo json_encode($data);
		}

		return false;
   }
   
   /*
	* Fetches the route value from the route table 
	* this function is called from the datatable ajax function
	*/
	public function fetchRoutesData()
	{
		$result = array('data' => array());

		$data = $this->model_routes->getRoutesData();

		foreach ($data as $key => $value) {

			// button
			$buttons = '';

			if(in_array('updateRoutes', $this->permission)) {
				$buttons .= '<button type="button" class="btn btn-default btn-sm" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
			}

			if(in_array('deleteRoutes', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
				

			$status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

			$result['data'][$key] = array(
            $value['name'],
            $value['coverage'],
            $value['loads'],
            $value['unit'],
            $value['return'],
				$value['reload'],
				$value['expenses'],
            // $value['date'],
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
   }
   /*
	* Its checks the routes form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		if(!in_array('createRoutes', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

      $this->form_validation->set_rules('route_name', 'Routes', 'trim|required');
      $this->form_validation->set_rules('coverage', 'Coverage', 'trim|required');
      $this->form_validation->set_rules('loads', 'Loads', 'trim|required');
      $this->form_validation->set_rules('units', 'Units', 'trim|required');
     $this->form_validation->set_rules('expenses', 'Expenses', 'required');
		$this->form_validation->set_rules('active', 'Active', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'name' => $this->input->post('route_name'),
            'coverage' => $this->input->post('coverage'),
            'loads' => $this->input->post('loads'),
            'unit' => $this->input->post('units'),
            'expenses' => $this->input->post('expenses'),
            'active' => $this->input->post('active'),	
        	);

        	$create = $this->model_routes->create($data);
        	if($create == true) {
        		$response['success'] = true;
        		$response['messages'] = 'Succesfully created';
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error while creating new route';			
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}
	
	/*
	* Its checks the routes form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function update($id)
	{

		if(!in_array('updateRoutes', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		if($id) {
			$this->form_validation->set_rules('edit_routes_name', 'Routes', 'trim|required');
			$this->form_validation->set_rules('edit_coverage_name', 'Coverage', 'trim|required');
			$this->form_validation->set_rules('edit_loads', 'Loads', 'trim|required');
			$this->form_validation->set_rules('edit_unit', 'Unit', 'trim|required');
			$this->form_validation->set_rules('edit_returns', 'Returns', 'trim|required');
			$this->form_validation->set_rules('edit_reloads', 'Reloads', 'trim|required');
			$this->form_validation->set_rules('edit_expenses', 'Expenses', 'trim|required');
			$this->form_validation->set_rules('edit_active', 'Active', 'trim|required');

			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
					'name' => $this->input->post('edit_routes_name'),
					'coverage' => $this->input->post('edit_coverage_name'),
					'loads' => $this->input->post('edit_loads'),
					'unit' => $this->input->post('edit_unit'),
					'return' => $this->input->post('edit_returns'),
					'reload' => $this->input->post('edit_reloads'),
					'expenses' => $this->input->post('edit_expenses'),
	        		'active' => $this->input->post('edit_active'),	
	        	);

	        	$update = $this->model_routes->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error while updating route';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}
	
	

   //Remove()
   /*
	* It removes the route information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deleteRoutes', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$route_id = $this->input->post('route_id');

      $response = array();
      //console.log($response);
		if($route_id) {
			$delete = $this->model_routes->remove($route_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error while removing route";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}

	/* */
	public function fetchRoutesDataDashboard()
	{
		$result = array('data' => array());

		$data = $this->model_routes->getRoutesData();

		foreach ($data as $key => $value) {

			//$routes_data = $this->model_routes->getRoutesData($value['routes_id']);
			$status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
			
			$result['data'][$key] = array(
				$value['name'],
				$value['loads'],
				$value['expenses'],
				$status
			);
		} // /foreach

		echo json_encode($result);
	}


}