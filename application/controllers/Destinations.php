<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Destinations extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Destinations';

	 	$this->load->model('model_destinations');
		// $this->load->model('model_destinations');
	
	}

	/* 
	* It only redirects to the manage destinations page
	* It passes the total destinations information
	into the frontend.
	*/
	public function index()
	{
		// $this->data['total_trucks'] = $this->model_trucks->countTotalTrucks();
		// $this->data['total_destinations'] = $this->model_destinations->countTotalDestinations();

		if(!in_array('viewTrucks', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$this->render_template('destinations/index', $this->data);
	}

	/*
	* It checks if it gets the trucks id and retreives
	* the trucks information from the trucks model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchDestinationsDataById($id) 
	{
		if($id) {
			$data = $this->model_destinations->getDestinationsData($id);
			echo json_encode($data);
		}

		return false;
   }
	//Destination list
	public function fetchDestinationsData()
	{
		$result = array('data' => array());

		$data = $this->model_destinations->getDestinationsData();

		foreach ($data as $key => $value) {

			$buttons = '';

			if(in_array('updateDestinations', $this->permission)) {
				$buttons .= '<button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-default btn-sm" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
			}

			if(in_array('deleteDestinations', $this->permission)) {
				$buttons .= ' <button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-default btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
			$status = ($value['status'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

			$result['data'][$key] = array(
				$value['name'],
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}
	
	//


	/* */
	public function fetchTrucksDataDashboard()
	{
		$result = array('data' => array());

		$data = $this->model_trucks->getTrucksData();

		foreach ($data as $key => $value) {

			//$routes_data = $this->model_routes->getRoutesData($value['routes_id']);
			// $status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
			
			$result['data'][$key] = array(
				$value['name'],
				// $value['status']
				// $value['expenses'],
				// $status
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* Its checks the destinations form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		if(!in_array('createDestinations', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		$this->form_validation->set_rules('destinations', 'Destination', 'trim|required');
		$this->form_validation->set_rules('status', 'Active', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'name' => $this->input->post('destinations'),
        		'status' => $this->input->post('status'),	
        	);

        	$create = $this->model_destinations->create($data);
        	if($create == true) {
        		$response['success'] = true;
				$response['messages'] = 'Succesfully created';
				  //redirect('trucks/', 'refresh');
        	}
        	else {
        		$response['success'] = false;
				$response['messages'] = 'Error while creating new truck';
				  //redirect('trucks/create', 'refresh');			
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}

	/*
	* Its checks the destinations form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function update($id)
	{

		if(!in_array('updateDestinations', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		if($id) {
         $this->form_validation->set_rules('edit_destinations', 'Destinations', 'trim|required');
         
         $this->form_validation->set_rules('edit_status', 'Active', 'trim|required');

			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
					
					'name' => $this->input->post('edit_destinations'),
               
	        		'status' => $this->input->post('edit_status'),	
	        	);

	        	$update = $this->model_destinations->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error while updating truck';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}

	//Remove()
   /*
	* It removes the destinations information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deleteDestinations', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$destinations_id = $this->input->post('destinations_id');

      $response = array();
		if($destinations_id) {
			$delete = $this->model_destinations->remove($destinations_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
				//redirect('trucks/', 'refresh');
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error while removing customer";
				//redirect('trucks/', 'refresh');
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
			//redirect('trucks/', 'refresh');
		}

		echo json_encode($response);
	}
}