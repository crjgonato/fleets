<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Operation extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Operation';

	 	$this->load->model('model_operation');
		$this->load->model('model_plateno');
		$this->load->model('model_destinations');
	
   }

   public function index()
	{
		

		if(!in_array('viewOperation', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

      $user_id = $this->session->userdata('id');
		$is_admin = ($user_id == 1 || $user_id == 6 ) ? true :false;
		// for plate number dropdown
		$plateno_data = $this->model_plateno->getActivePlatenoData();
		$this->data['plateno_data'] = $plateno_data;
		// for destionation dropdown 
		$destinations_data = $this->model_destinations->getActiveDestinationsData();
		$this->data['destinations_data'] = $destinations_data;

		$this->data['is_admin'] = $is_admin;
		$this->render_template('operation/index', $this->data);
   }

   public function success()
	{
		if(!in_array('viewOperation', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

      $user_id = $this->session->userdata('id');
		$is_admin = ($user_id == 1 || $user_id == 6 ) ? true :false;

		$this->data['is_admin'] = $is_admin;
		
		$this->render_template('operation/success', $this->data);
   }
   public function fetchOperationDataById($id) 
	{
		if($id) {
			$data = $this->model_operation->getOperationData($id);
			echo json_encode($data);
		}

		return false;
   }
   //
   public function create()
	{
		if(!in_array('createOperation', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();
      $this->form_validation->set_rules('barcode', 'Barcode', 'trim|required');
		$this->form_validation->set_rules('plateno', 'Plate Number', 'trim|required');
		$this->form_validation->set_rules('gasoline', 'Gasoline', 'trim|required');
      $this->form_validation->set_rules('destination', 'Destination', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('time', 'Time', 'trim|required');
		$this->form_validation->set_rules('date_added', 'Date', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
				'barcode' => $this->input->post('barcode'),
				'gasoline' => $this->input->post('gasoline'),
            'plateno' => $this->input->post('plateno'),
            'destination' => $this->input->post('destination'),
				'status' => $this->input->post('status'),	
				'time' => $this->input->post('time'),
				'date_added' => $this->input->post('date_added'),	
        	);

        	$create = $this->model_operation->create($data);
        	if($create == true) {
        		$response['success'] = true;
				  $response['messages'] = 'Succesfully created';
              redirect('operation/success', 'refresh');
              
        	}
        	else {
        		$response['success'] = false;
				  $response['messages'] = 'Error while creating new truck';
              redirect('operation/', 'refresh');	
              //alert('Error');		
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}

}