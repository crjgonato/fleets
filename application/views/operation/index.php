<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
<link rel="stylesheet" href="//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css"> -->
<!-- <meta http-equiv="refresh" content="40;"/>  -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php if($is_admin == false): ?>
        <div class="row">
          <div class="col-lg-7 col-xs-12">
              <div class="box login-box">
                  <div class="login-box-body">
                     <form action="<?php echo base_url('operation/create') ?>" method="post">
                        <div class="form-group has-feedback">
                           <!-- <a class="btn btn-warning btn-flat btn-sm" id="startButton">SCAN CODE</a>
                           <a class="btn btn-warning btn-block btn-flat btn-sm hidden" id="resetButton">Reset</a> -->
                        <div>
                  </div>
               <!-- <div class="videoframe">
                  <video id="video"  width="310"  style="border: 1px solid gray"></video>
               </div> -->

               <!-- <div id="sourceSelectPanel" style="display:none">
                     <label for="sourceSelect">Change video source:</label>
                     <select id="sourceSelect" style="max-width:310px">
                     </select>
               </div> -->

               <label class="">Control number</label>
               <input type="text" class="form-control input-md capitalize" name="barcode" id="barcode" placeholder="" required>
               <blockquote class="hidden">
                     <p  id="result" name="result"></p>
                     
               </blockquote>
            </div>
            <div class="form-group has-feedback">
               <label for="plateno">Plate Number</label>
                <select class="form-control input-md" name="plateno" id="plateno" required>
                <!-- <option value="ABC 123">ABC 123</option>
                <option value="DEF 456">DEF 456</option>
                <option value="GHI 789">GHI 789</option> -->
                <option value=""></option>
                        <?php foreach ($plateno_data as $k => $v): ?>
                        <option value="<?php echo $v['name'] ?>"><?php echo $v['name'] ?></option>
                        <?php endforeach ?>
                </select>
            </div>
            <div class="form-group has-feedback">
               <label for="destination">Destination</label>
            <select class="form-control input-md" name="destination" id="destination" required>
               <!-- <option></option>
               <option value="Cebu">Cebu</option> -->
               <option value=""></option>
                    <?php foreach ($destinations_data as $k => $v): ?>
                      <option value="<?php echo $v['name'] ?>"><?php echo $v['name'] ?></option>
                    <?php endforeach ?>
            </select>
            </div>
            <div class="form-group has-feedback">
               <label for="gasoline">Gasoline</label>
               <input type="text" class="form-control input-md capitalize" name="gasoline" id="gasoline">
            </div>
      
            <div class="form-group has-feedback">
               <div class="form-group">
                     <label for="status">Status</label>
                     <select class="form-control input-md" id="status" name="status" required>
                     <option></option>
                     <option value="Time in">Time in</option>
                     <option value="Time out">Time out</option>
                     </select>
               </div>
            </div>
            <div class="row">
            
               <div class="col-lg-12">
                  <input type="text" class="hidden" id="time" name="time" value="<?php
                  date_default_timezone_set('Asia/Manila');
                   echo date('h:i:a'); ?>">
                   <input type="text" class="hidden" id="date_added" name="date_added" value="<?php
                  date_default_timezone_set('Asia/Manila');
                   echo date('d-m-Y'); ?>">
                  <button type="submit" class="btn btn-warning btn-block btn-flat btn-lg">Send </button>
               </div>
          
            </div>
               </form>
</div>
  <!-- /.login-box-body -->
</div>
            </div>  
        </div>
      <?php endif; ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    //$(".col-md-6").slideToggle(2500);
   

    $(document).ready(function() {
      $("#operationMainMenu").addClass('active');

      // $('#startButton').click(function(){
      //    $('.videoframe').removeAttr('hidden');
      // });
      
    });

    // submit the create from 
$("#createForm").unbind('submit').on('submit', function() {
    var form = $(this);

    // remove the text-danger
    $(".text-danger").remove();

    $.ajax({
      url: form.attr('action'),
      type: form.attr('method'),
      data: form.serialize(), // /converting the form data into array and sending it to server
      dataType: 'json',
      success:function(response) {

        //manageTable.ajax.reload(null, false); 

        
      }
    }); 

    return false;
  });
});
</script>

<!-- <script type="text/javascript">
    window.addEventListener('load', function() {
        const codeReader = new Library.BrowserBarcodeReader()
        console.log('ZXing code reader initialized')
        codeReader.getVideoInputDevices()
            .then((videoInputDevices) => {
            const sourceSelect = document.getElementById('sourceSelect')
            const firstDeviceId = videoInputDevices[0].deviceId
            if (videoInputDevices.length > 1) {
            videoInputDevices.forEach((element) => {
                const sourceOption = document.createElement('option')
                sourceOption.text = element.label
            sourceOption.value = element.deviceId
            sourceSelect.appendChild(sourceOption)
        })
            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
        }
        document.getElementById('startButton').addEventListener('click', () => {
            codeReader.decodeFromInputVideoDevice(firstDeviceId, 'video').then((result) => {
            console.log(result)

        document.getElementById('barcode').value = result.text
        
        
    }).catch((err) => {
            console.error(err)
        document.getElementById('result').textContent = err
    })
        console.log(`Started continous decode from camera with id ${firstDeviceId}`)
    })
        document.getElementById('resetButton').addEventListener('click', () => {
            document.getElementById('result').textContent = '';
            codeReader.reset();
        //console.log('Reset.')
    })
    })
    .catch((err) => {
            console.error(err)
    })
    })
</script> -->
<style>
.capitalize {
    text-transform: uppercase !important;
}
</style>

