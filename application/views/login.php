<!DOCTYPE html>
<html oncontextmenu="return false;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>FleetsHub | Sign in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  
  
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/square/blue.css') ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="icon" href="<?php echo base_url('./assets/favicon.ico') ?>" type="image/x-icon">
  <!-- Google Font -->
  <link rel="stylesheet" href="<?php echo base_url('./assets/dist/css/fonts.css') ?>">
  <style>
    .nextcentered p {
      font-size: xx-small;
    }
    .footer {
    position: fixed;
    float: right;
    right: 0;
    bottom: 0;
    left: 0;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-right: 20px;
    /* padding: 1rem; */
    background-color:  transparent;
    text-align: right;
    font-size: 13px;
}
  </style>
</head>
<body class="hold-transition login-page">
<div class="box login-box">
  <div class="login-logo">
    <!-- <a href="<//?php echo base_url('auth'); ?>"> -->
    <a href="#">
      <h3><strong><font color="#607D8B">FleetsHub</font> </strong></h3>
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h4 class="login-box-msg">Sign in</h4>
    <p class="login-box-msg">with your FleetsHub account.</p>
    <form action="<?php echo base_url('auth/login') ?>" method="post">
      <div class="form-group has-feedback">
        <!-- <input type="email" class="form-control input-sm" name="email" id="email" placeholder="Email" value="admin@su.co" autocomplete="off"> -->
        <input type="email" class="form-control input-sm" name="email" id="email" placeholder="Email" value="admin@su.co" autocomplete="off" autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control input-sm" name="password" id="password" placeholder="Password" value="Programm3r1" autocomplete="off">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
              <!-- <input type="checkbox"> Remember Me -->
              &nbsp;
              <font color="red">
                <?php echo validation_errors(); ?>  
                <?php if(!empty($errors)) {
                  echo $errors;
                } ?>
              </font>
           
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-warning btn-block btn-flat btn-sm">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
      <div class="nextcentered">
        <p class="pull-right"><i>v1.85.26</i></p>
        </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<div class="footer">© 2018 <b>FleetsHub</b> | Powered by <b>Adccurate</b> | <a href="mailto:crjgonato@gmail.com"><font color="#607D8B">Help</font></a></div>
<!-- jQuery 3 -->

<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js') ?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script>
  $(document).ready(function(event){
    $(function() {
      $('body').removeClass('fade-out');
    });
  });
  
  $(document).keydown(function(event){
      if(event.keyCode==123){
          return false;
      }
      else if (event.ctrlKey && event.shiftKey && event.keyCode==73){        
              return false;
      }
  });

  $(document).on("contextmenu",function(e){        
    e.preventDefault();
  });

  // $('.alert').delay(5000).fadeOut(5000)


  // var selector = '.navi li';
  // $(selector).on('click', function(){
  //     $(selector).removeClass('active');
  //     $(this).addClass('active');
  // });
</script>
</body>
</html>
