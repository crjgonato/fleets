

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
      <small>Master List</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Master List</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <!-- <//?php if(in_array('createTrucks', $user_permission)): ?>
          <button class="btn btn-warning btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addModal">Add new Plate Number</button>
          <br /> <br />
        <//?php endif; ?> -->

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Manage Master list</h3>
          </div>
          <!-- /.box-header -->
          <div class="">
                <div class="box-body">
                <table id="manageTable" class="table">
                    <thead>
                      <tr>
                        <th>Barcodes</th>
                        <!-- <th>Trucks</th> -->
                        <th>Plate Numbers</th>
                        <th class="capitalize">Gasoline</th>
                        <th>Destinations</th>
                        <th>Dates</th>
                        <th>Times</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                </table>
                </div>
              </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<?php if(in_array('updateTrucks', $user_permission)): ?>
<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit a Plate Number</h4>
      </div>

      <form role="form" action="<?php echo base_url('plateno/update') ?>" method="post" id="updateForm">

        <div class="modal-body">
          <div id="messages"></div>

          <div class="form-group">
            <label for="edit_plateno">Plate Number</label>
            <input type="text" class="form-control input-sm" id="edit_plateno" name="edit_plateno" placeholder="Enter plate number" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_status">Status</label>
            <select class="form-control input-sm" id="edit_status" name="edit_status">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Apply changes</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
          
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>

<?php if(in_array('deleteMasterlist', $user_permission)): ?>
<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete a list</h4>
      </div>

      <form role="form" action="<?php echo base_url('masterlist/remove') ?>" method="post" id="removeForm">
        <div class="modal-body">
          <p>Do you really want to delete?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Delete</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>


<script type="text/javascript">
    var manageTable;
    var base_url = "<?php echo base_url(); ?>";

$(document).ready(function() {
  $("#manageTable").addClass('active');
  
  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
        'ajax': base_url + 'masterlist/fetchMasterlistDataDashboard',
        'masterlist': []
  });

});

// edit function
// function editFunc(id)
// { 
//   $.ajax({
//     url: 'fetchPlatenoDataById/'+id,
//     type: 'post',
//     dataType: 'json',
//     success:function(response) {

//       $("#edit_plateno").val(response.name);
//       $("#edit_status").val(response.status);

//       //console.log(response);
//       // submit the edit from 
//       $("#updateForm").unbind('submit').bind('submit', function() {
//         var form = $(this);

//         // remove the text-danger
//         $(".text-danger").remove();

//         $.ajax({
//           url: form.attr('action') + '/' + id,
//           type: form.attr('method'),
//           data: form.serialize(), // /converting the form data into array and sending it to server
//           dataType: 'json',
//           success:function(response) {

//             manageTable.ajax.reload(null, false); 

//             if(response.success === true) {
//               $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
//                 '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
//                 '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
//               '</div>');


//               // hide the modal
//               $("#editModal").modal('hide');
//               // reset the form 
//               $("#updateForm .form-group").removeClass('has-error').removeClass('has-success');

//             } else {

//               if(response.messages instanceof Object) {
//                 $.each(response.messages, function(index, value) {
//                   var id = $("#"+index);

//                   id.closest('.form-group')
//                   .removeClass('has-error')
//                   .removeClass('has-success')
//                   .addClass(value.length > 0 ? 'has-error' : 'has-success');
                  
//                   id.after(value);

//                 });
//               } else {
//                 $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
//                   '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
//                   '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
//                 '</div>');
//               }
//             }
//           }
//         }); 

//         return false;
//       });

//     }
//   });
// }
function removeFunc(id)
{
  if(id) {
    $("#removeForm").on('submit', function() {

      var form = $(this);

      // remove the text-danger
      $(".text-danger").remove();

      $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: { masterlist_id:id }, 
        dataType: 'json',
        success:function(response) {
         console.log(response);
          manageTable.ajax.reload(null, false); 

          if(response.success === true) {
            $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
            '</div>');

            // hide the modal
            $("#removeModal").modal('hide');

          } else {

            $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
            '</div>'); 
          }
        }
      }); 

      return false;
    });
  }
}


</script>
<style>
.capitalize {
    text-transform: uppercase !important;
}
</style>
