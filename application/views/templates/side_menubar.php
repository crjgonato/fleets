<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        
     
      
    
        <!-- TODO -->
        <li class="header">Main Menu</li>
        <?php if(in_array('viewDashboard', $user_permission)): ?>
            <li id="dashboardMainMenu">
            <a href="<?php echo base_url('dashboard/') ?>">
              <i class="fa fa-home"></i> <span>Dashboard</span>
            </a>
            </li>
          <?php endif; ?>

          <?php if(in_array('viewDashboard', $user_permission)): ?>
            <li id="masterlistMainMenu">
            <a href="<?php echo base_url('masterlist/') ?>">
              <i class="fa fa-tasks"></i> <span>Master List</span>
            </a>
            </li>
          <?php endif; ?>

          

          <!-- <//?php if(in_array('viewDashboard', $user_permission)): ?>
            <li id="dashboardMainMenu">
            <a href="<//?php echo base_url('dashboard/') ?>">
              <i class="fa fa-barcode"></i> <span>Barcodes</span>
            </a>
            </li>
          <//?php endif; ?> -->

          <?php if(in_array('viewExpenses', $user_permission)): ?>
            <li id="dashboardMainMenu">
            <a href="<?php echo base_url('dashboard/') ?>">
              <i class="fa fa-file-excel-o"></i> <span>Expenses</span>
            </a>
            </li>
          <?php endif; ?>

          <?php if(in_array('viewOperation', $user_permission)): ?>
            <li id="operationMainMenu">
            <a href="<?php echo base_url('operation/') ?>">
              <i class="fa fa-barcode"></i> <span>Operation</span>
            </a>
            </li>
          <?php endif; ?>
          

          <?php if(in_array('createDestinations', $user_permission) || in_array('updateDestinations', $user_permission) || in_array('viewDestinations', $user_permission) || in_array('deleteDestinations', $user_permission)): ?>
            <li id="DestinationsNav">
            <a href="<?php echo base_url('destinations/') ?>">
              <i class="fa fa-road"></i> <span>Destinations</span>
            </a>
            </li>
          <?php endif; ?>

          <!-- <//?php if(in_array('createTruckgroup', $user_permission) || in_array('updateTruckgroup', $user_permission) || in_array('viewTruckgroup', $user_permission) || in_array('deleteTruckgroup', $user_permission)): ?>
            <li id="truckgroupNav">
            <a href="<//?php echo base_url('truckgroup/') ?>">
              <i class="fa fa-object-group"></i> <span>Groups</span>
            </a>
            </li>
          <//?php endif; ?> -->

          <?php if(in_array('createTrucks', $user_permission) || in_array('updateTrucks', $user_permission) || in_array('viewTrucks', $user_permission) || in_array('deleteTrucks', $user_permission)): ?>
            <li id="trucksNav">
            <a href="<?php echo base_url('trucks/') ?>">
              <i class="fa fa-truck"></i> <span>Trucks</span>
            </a>
            </li>
          <?php endif; ?>

          <?php if(in_array('createPlateno', $user_permission) || in_array('updatePlateno', $user_permission) || in_array('viewPlateno', $user_permission) || in_array('deletePlateno', $user_permission)): ?>
            <li id="platenoNav">
            <a href="<?php echo base_url('plateno/') ?>">
              <i class="fa fa-car"></i> <span>Plate Numbers</span>
            </a>
            </li>
          <?php endif; ?>
   
        <li class="header">System Preference</li>
        <?php if(in_array('updateSetting', $user_permission)): ?>
            <li class="treeview" id="mainSettingsNav">
            <a href="#">
              <i class="fa fa-gears"></i>
              <span>Settings</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(in_array('updateCompany', $user_permission)): ?>
                <li id="companyNav"><a href="<?php echo base_url('company/') ?>"><i class="fa fa-briefcase"></i> <span>Company</span></a></li>
              <?php endif; ?>

              <?php if(in_array('viewProfile', $user_permission)): ?>
                <li id="profNav"><a href="<?php echo base_url('users/profile/') ?>"><i class="fa fa-user-o"></i> <span>Profile</span></a></li>
              <?php endif; ?>

              <?php if(in_array('createUser', $user_permission) || in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>

                <li class="treeview" id="mainUserNav">
            <a href="#">
              <i class="ion ion-android-people"></i>
              <span>Users</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(in_array('createUser', $user_permission)): ?>
              <li id="createUserNav"><a href="<?php echo base_url('users/create') ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
              <?php endif; ?>

              <?php if(in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
              <li id="manageUserNav"><a href="<?php echo base_url('users') ?>"><i class="fa fa-circle-o"></i> Manage Users</a></li>
            <?php endif; ?>
            </ul>
          </li>
              <?php endif; ?>
              <?php if(in_array('createGroup', $user_permission) || in_array('updateGroup', $user_permission) || in_array('viewGroup', $user_permission) || in_array('deleteGroup', $user_permission)): ?>
            <li class="treeview" id="mainGroupNav">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>User Groups</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createGroup', $user_permission)): ?>
                  <li id="addGroupNav"><a href="<?php echo base_url('groups/create') ?>"><i class="fa fa-circle-o"></i> Add Group</a></li>
                <?php endif; ?>
                <?php if(in_array('updateGroup', $user_permission) || in_array('viewGroup', $user_permission) || in_array('deleteGroup', $user_permission)): ?>
                <li id="manageGroupNav"><a href="<?php echo base_url('groups') ?>"><i class="fa fa-circle-o"></i> Manage Groups</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>

            </ul>
            
          </li>
          <?php endif; ?>
          



        
        <!-- user permission info -->
        <li><a href="<?php echo base_url('auth/logout') ?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>