<meta http-equiv="refresh" content="60" /> 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php if($is_admin == true): ?>
      <section class="content-header">
        <h1>
          Home
          <small>Dashboard</small>
        </h1>
        <ol class="breadcrumb">
          <!-- <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li> -->
          <!-- <li class="active">Dashboard</li> -->

        </ol>
      </section>
    <?php endif; ?>

    <?php if($is_admin == false): ?>
      <section class="content-header">
          <h1>
            Home
            <small>Dashboard</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
          
          </ol>
        </section>
    <?php endif; ?>
    

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php if($is_admin == true): ?>
        <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-file-text-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Master List</span>
                <span class="info-box-number"><?php echo $total_masterlist ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>            
          </div>
          
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-road"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Destinations</span>
                <span class="info-box-number"><?php echo $total_destinations ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>            
          </div>

          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-truck"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Trucks</span>
                <span class="info-box-number"><?php echo $total_trucks ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>            
          </div>
          <!-- ./col -->
          
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-car"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Plate Numbers</span>
                <span class="info-box-number"><?php echo $total_plateno ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>            
          </div>
          <!-- ./col -->
          
          
        </div>

        
        <br>
        <!-- 1st table box -->
        <div class="row">
          <!-- <div class="col-lg-6 hidden">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h4 class="box-title">Trucks</h4>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="dashboard_salesbycity">
                <div class="box-body ">
                  <table id="manageTabletrucks" class="table ">
                    <thead>
                      <tr>
                        <th>Trucks IDs</th>
            
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div> -->
         
          <div class="col-lg-12">
            

            <div class="box box-warning">
              <div class="box-header with-border">
                <h4 class="box-title">Master List</h4>
                <h4 class="box-title pull-right">
                  
                  <?php
                    date_default_timezone_set('Asia/Manila'); // setting timezone
                    $monday = strtotime("last monday");
                    $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
                    $sunday = strtotime(date("d-m-Y",$monday)." +6 days");
                    $this_week_sd = date("d-m-Y",$monday);
                    $this_week_ed = date("d-m-Y",$sunday);
                    echo "Current week (from <b>$this_week_sd</b> to <b>$this_week_ed</b>) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                  ?>
                </h4>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="dashboard_salesbycity">
                <div class="box-body">
                <table id="manageTablemasterlist" class="table">
                    <thead>
                      <tr>
                        <th>Barcodes</th>
                        <th>Plate Numbers</th>
                        <th class="capitalize">Gasoline</th>
                        <th>Destinations</th>
                        <th>Dates</th>
                        <th>Times</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      <?php endif; ?>

      <?php if($is_admin == false): 
        redirect('operation/', '');
      ?>
        <div class="row">
          <div class="col-lg-7 col-xs-12">
              <div class="info-box">
                <div class="info-box-content">
                  <h4>Welcome to FleetsHub, A Fleet Management System</h4>
                   <!-- <small>Use the left side menu to navigate</small> -->
                </div>
              </div>
            </div>  
        </div>
      <?php endif; ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    //$(".col-md-6").slideToggle(2500);
    //var manageTabletrucks;
    var manageTablemasterlist;

    // var order = table.order();
    // var title = table.column(order[0][0]).header();

    var base_url = "<?php echo base_url(); ?>";
    $(document).ready(function() {
      $("#dashboardMainMenu").addClass('active');

      // initialize the datatable for Sales
      // manageTabletrucks = $('#manageTabletrucks').DataTable({
      //   'ajax': base_url + 'trucks/fetchTrucksDataDashboard',
      //   'trucks': []
      // });
      // initialize the datatable for Sales
      manageTablemasterlist = $('#manageTablemasterlist').DataTable({
        'ajax': base_url + 'masterlist/fetchMasterlistDataDashboard',
        'masterlist': [

            { "data": "barcode" },
            { "data": "plateno" },
            { "data": "gasoline" },
            { "data": "destination" },
            { "data": "date" },
            { "data": "time" }
            // { "data": "salary" }
          
        ],
        order: [
      [4, 'asc'],[5, 'asc']
    ]
      //   order: [
      // [3, 'asc']
      });
      
      //console.log(manageTablemasterlist);
      var order = table.order();
      var title = table.column(order[0][0]).header();
      
    });
</script>
<style>
.capitalize {
    text-transform: uppercase !important;
}
</style>