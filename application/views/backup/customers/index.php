<!-- <//?php
   echo('Hello World');
?> -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
      <small>Customers</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Customers</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
     <!-- <h1>HELLO WORLD</h1> -->
     <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <?php if(in_array('createCustomers', $user_permission)): ?>
          <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#addModal">Add Customer</button>
          <br /> <br />
        <?php endif; ?>

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Manage Customers</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table ">
              <thead>
              <tr>
                <th>CCodes</th>
                <th>Business</th>
                <th>Names</th>
                <th>Cities</th>
                <th>Contacts</th>
                <th>Segments</th>
                <!-- <th>Address</th> -->
                <!-- <th>Zipcodes</th> -->
                <th>Terms</th>
                <th>Visits</th>
                <th>Status</th>
                <?php if(in_array('updateCustomers', $user_permission) || in_array('deleteCustomers', $user_permission)): ?>
                  <th>Action</th>
                <?php endif; ?>
              </tr>
              </thead>

            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
   </section>
</div>

<?php if(in_array('createCustomers', $user_permission)): ?>
<!-- create customer modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Customer</h4>
      </div>

      <form role="form" action="<?php echo base_url('customers/create') ?>" method="post" id="createForm">

        <div class="modal-body">

          <div class="form-group">
            <label for="customer_code">Customer Code</label>
            <input type="text" class="form-control input-sm" id="customer_code" name="customer_code" placeholder="Enter customer code" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="biz_name">Business Name</label>
            <input type="text" class="form-control input-sm" id="biz_name" name="biz_name" placeholder="Enter business name" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control input-sm" id="name" name="name" placeholder="Enter fullname" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="city">City</label>
            <input type="text" class="form-control input-sm" id="city" name="city" placeholder="Enter city" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="mob_num">Contact</label>
            <input type="text" class="form-control input-sm" id="mob_num" name="mob_num" placeholder="Enter contact number" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="segment">Segment</label>
            <input type="text" class="form-control input-sm" id="segment" name="segment" placeholder="Enter segment" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="street_home_ofc">Address</label>
            <input type="text" class="form-control input-sm" id="street_home_ofc" name="street_home_ofc" placeholder="Enter address" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="zipcode">Zipcode</label>
            <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Enter zipcode" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="term">Term</label>
            <input type="text" class="form-control input-sm" id="term" name="term" placeholder="Enter term" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="dsp_visit">Visit</label>
            <input type="text" class="form-control input-sm" id="dsp_visit" name="dsp_visit" placeholder="Enter visit" autocomplete="off">
          </div>
         
          

          <div class="form-group">
            <label for="active">Status</label>
            <select class="form-control input-sm" id="active" name="active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Add Customer</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
          
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>

<?php if(in_array('updateCategory', $user_permission)): ?>
<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Customer</h4>
      </div>

      <form role="form" action="<?php echo base_url('customers/update') ?>" method="post" id="updateForm">

        <div class="modal-body">
          <div id="messages"></div>

          <div class="form-group">
            <label for="edit_customer_code">Customer Code</label>
            <input type="text" class="form-control input-sm" id="edit_customer_code" name="edit_customer_code" placeholder="Enter customer code" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_biz_name">Business Name</label>
            <input type="text" class="form-control input-sm" id="edit_biz_name" name="edit_biz_name" placeholder="Enter business name" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_name">Name</label>
            <input type="text" class="form-control input-sm" id="edit_name" name="edit_name" placeholder="Enter fullname" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_city">City</label>
            <input type="text" class="form-control input-sm" id="edit_city" name="edit_city" placeholder="Enter city" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_mob_num">Contact</label>
            <input type="text" class="form-control input-sm" id="edit_mob_num" name="edit_mob_num" placeholder="Enter contact" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_segment">Segment</label>
            <input type="text" class="form-control input-sm" id="edit_segment" name="edit_segment" placeholder="Enter segment" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_street_home_ofc">Address</label>
            <input type="text" class="form-control input-sm" id="edit_street_home_ofc" name="edit_street_home_ofc" placeholder="Enter address" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_zipcode">Zipcode</label>
            <input type="text" class="form-control input-sm" id="edit_zipcode" name="edit_zipcode" placeholder="Enter zipcode" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_term">Term</label>
            <input type="text" class="form-control input-sm" id="edit_term" name="edit_term" placeholder="Enter term" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_dsp_visit">Visit</label>
            <input type="text" class="form-control input-sm" id="edit_dsp_visit" name="edit_dsp_visit" placeholder="Enter visit" autocomplete="off">
          </div>
         
          <div class="form-group">
            <label for="edit_active">Status</label>
            <select class="form-control input-sm" id="edit_active" name="edit_active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Save changes</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
          
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>


<?php if(in_array('deleteCustomers', $user_permission)): ?>
<!-- remove customer modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete Customer</h4>
      </div>

      <form role="form" action="<?php echo base_url('customers/remove') ?>" method="post" id="removeForm">
        <div class="modal-body">
          <p>Do you really want to delete?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Delete</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>



<script type="text/javascript">
   var manageTable;

   $(document).ready(function() {
      $("#customersNav").addClass('active');

      // initialize the datatable 
      manageTable = $('#manageTable').DataTable({
         'ajax': 'fetchCustomersData',
         'order': []
      });

      //createForm
      // submit the create from 
      $("#createForm").unbind('submit').on('submit', function() {
         var form = $(this);

         // remove the text-danger
         $(".text-danger").remove();

         $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(), // /converting the form data into array and sending it to server
            dataType: 'json',
            success:function(response) {

            manageTable.ajax.reload(null, false); 

            if(response.success === true) {
               $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
               '</div>');


               // hide the modal
               $("#addModal").modal('hide');

               // reset the form
               $("#createForm")[0].reset();
               $("#createForm .form-group").removeClass('has-error').removeClass('has-success');

            } else {

               if(response.messages instanceof Object) {
                  $.each(response.messages, function(index, value) {
                  var id = $("#"+index);

                  id.closest('.form-group')
                  .removeClass('has-error')
                  .removeClass('has-success')
                  .addClass(value.length > 0 ? 'has-error' : 'has-success');
                  
                  id.after(value);

                  });
               } else {
                  $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
                  '</div>');
               }
            }
            }
         }); 

         return false;
      });
      //editForm()
      
      
   
   });

   // edit function
function editFunc(id)
{ 
  $.ajax({
    url: 'fetchCustomersDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {

      $("#edit_customer_code").val(response.customer_code);
      $("#edit_biz_name").val(response.biz_name);
      $("#edit_name").val(response.name);
      $("#edit_city").val(response.city);
      $("#edit_mob_num").val(response.mob_num);
      $("#edit_segment").val(response.segment);
      $("#edit_street_home_ofc").val(response.street_home_ofc);
      $("#edit_zipcode").val(response.zipcode);
      $("#edit_term").val(response.term);
      $("#edit_dsp_visit").val(response.dsp_visit);
      $("#edit_active").val(response.active);

      // submit the edit from 
      $("#updateForm").unbind('submit').bind('submit', function() {
        var form = $(this);

        // remove the text-danger
        $(".text-danger").remove();

        $.ajax({
          url: form.attr('action') + '/' + id,
          type: form.attr('method'),
          data: form.serialize(), // /converting the form data into array and sending it to server
          dataType: 'json',
          success:function(response) {

            manageTable.ajax.reload(null, false); 

            if(response.success === true) {
              $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
              '</div>');


              // hide the modal
              $("#editModal").modal('hide');
              // reset the form 
              $("#updateForm .form-group").removeClass('has-error').removeClass('has-success');

            } else {

              if(response.messages instanceof Object) {
                $.each(response.messages, function(index, value) {
                  var id = $("#"+index);

                  id.closest('.form-group')
                  .removeClass('has-error')
                  .removeClass('has-success')
                  .addClass(value.length > 0 ? 'has-error' : 'has-success');
                  
                  id.after(value);

                });
              } else {
                $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
                '</div>');
              }
            }
          }
        }); 

        return false;
      });

    }
  });
}

   // remove functions 
   function removeFunc(id)
   {
      if(id) {
         $("#removeForm").on('submit', function() {

            var form = $(this);

            // remove the text-danger
            $(".text-danger").remove();

            $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: { customers_id:id }, 
            dataType: 'json',
            success:function(response) {

               manageTable.ajax.reload(null, false); 

               if(response.success === true) {
                  $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
                  '</div>');

                  // hide the modal
                  $("#removeModal").modal('hide');

               } else {

                  $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
                  '</div>'); 
               }
            }
            }); 

            return false;
         });
      }
   }

</script>