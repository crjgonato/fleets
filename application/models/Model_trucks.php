<?php 

class Model_trucks extends CI_Model 
{
   public function __construct()
	{
		parent::__construct();
   }

   /* get active trucks infromation */
	public function getActiveTrucks()
	{
		$sql = "SELECT * FROM trucks WHERE status = ?";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }

   /* get the trucks data */
	public function getTrucksData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM trucks WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM trucks";
		$query = $this->db->query($sql);
		return $query->result_array();
   }
   
   public function getActiveTrucksData()
	{
		$sql = "SELECT * FROM trucks WHERE status = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }
   
   
   // Counts
   public function countTotalTrucks()
	{
		$sql = "SELECT * FROM trucks WHERE status = '1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
   }

   // public function countTotalDestinations()
	// {
	// 	$sql = "SELECT * FROM destinations WHERE status = '1'";
	// 	$query = $this->db->query($sql);
	// 	return $query->num_rows();
	// }

   public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('trucks', $data);
			return ($insert == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('trucks', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('trucks');
			return ($delete == true) ? true : false;
		}
	}
}