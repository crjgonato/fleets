<?php 

class Model_customers extends CI_Model 
{
   public function __construct()
	{
		parent::__construct();
   }

   /* get active customers infromation */
	public function getActiveCustomers()
	{
		$sql = "SELECT * FROM customers WHERE active = ?";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }

   /* get the customers data */
	public function getCustomersData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM customers WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM customers";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getActiveCustomersData()
	{
		$sql = "SELECT * FROM customers WHERE active = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}

   public function countTotalCustomers()
	{
		$sql = "SELECT * FROM customers WHERE active = '1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
   }
   
    //Create()
    public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('customers', $data);
			return ($insert == true) ? true : false;
		}
	}
    //Update()
    public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('customers', $data);
			return ($update == true) ? true : false;
		}
	}
    //Remove()
   public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('customers');
			return ($delete == true) ? true : false;
		}
	}


}  