<?php 

class Model_masterlist extends CI_Model 
{
   public function __construct()
	{
		parent::__construct();
   }

   /* get active trucks infromation */
	public function getActiveMasterlist()
	{
		$sql = "SELECT * FROM masterlist ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }

   /* get the trucks data */
	public function getMasterlistData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM masterlist WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM masterlist ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
   }
   
   public function getActiveMasterlistData()
	{
		$sql = "SELECT * FROM masterlist ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }
   
   // Counts
   public function countTotalMasterlist()
	{
		$sql = "SELECT * FROM masterlist";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
   // Counts
   // public function countTotalMasterlist()
	// {
	// 	$sql = "SELECT * FROM masterlist WHERE status = '1'";
	// 	$query = $this->db->query($sql);
	// 	return $query->num_rows();
   // }


   // public function create($data)
	// {
	// 	if($data) {
	// 		$insert = $this->db->insert('trucks', $data);
	// 		return ($insert == true) ? true : false;
	// 	}
	// }

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('trucks', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('masterlist');
			return ($delete == true) ? true : false;
		}
	}
}