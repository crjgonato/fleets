<?php 

class Model_destinations extends CI_Model 
{
   public function __construct()
	{
		parent::__construct();
   }

   /* get active trucks infromation */
	public function getActiveDestinations()
	{
		$sql = "SELECT * FROM destinations WHERE status = ?";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }

   /* get the trucks data */
	public function getDestinationsData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM destinations WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM destinations";
		$query = $this->db->query($sql);
		return $query->result_array();
   }
   
   public function getActiveDestinationsData()
	{
		$sql = "SELECT * FROM destinations WHERE status = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }
   
   
   // Counts
   public function countTotalDestinations()
	{
		$sql = "SELECT * FROM destinations WHERE status = '1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
   }

	//Create()
	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('destinations', $data);
			return ($insert == true) ? true : false;
		}
	}
	//Update()
	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('destinations', $data);
			return ($update == true) ? true : false;
		}
	}
	//Remove()
	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('destinations');
			return ($delete == true) ? true : false;
		}
	}
}