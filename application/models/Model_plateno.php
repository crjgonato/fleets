<?php 

class Model_plateno extends CI_Model 
{
   public function __construct()
	{
		parent::__construct();
   }

   /* get active trucks infromation */
	public function getActivePlateno()
	{
		$sql = "SELECT * FROM plateno WHERE status = ?";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }

   /* get the trucks data */
	public function getPlatenoData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM plateno WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM plateno";
		$query = $this->db->query($sql);
		return $query->result_array();
   }
   
   public function getActivePlatenoData()
	{
		$sql = "SELECT * FROM plateno WHERE status = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }
   
   
   // Counts
   public function countTotalPlateno()
	{
		$sql = "SELECT * FROM plateno WHERE status = '1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
   }

	
	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('plateno', $data);
			return ($insert == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('plateno', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('plateno');
			return ($delete == true) ? true : false;
		}
	}
}