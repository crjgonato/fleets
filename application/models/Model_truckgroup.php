<?php 

class Model_truckgroup extends CI_Model 
{
   public function __construct()
	{
		parent::__construct();
   }

   /* get active trucks infromation */
	public function getActiveTruckGroup()
	{
		$sql = "SELECT * FROM truckgroup WHERE status = ?";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }

   /* get the trucks data */
	public function getTruckGroupData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM truckgroup WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM truckgroup";
		$query = $this->db->query($sql);
		return $query->result_array();
   }
   
   public function getActiveTruckGroupData()
	{
		$sql = "SELECT * FROM truckgroup WHERE status = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }
   
   
   // Counts
   public function countTotalTruckGroup()
	{
		$sql = "SELECT * FROM truckgroup WHERE status = '1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
   }

   // public function countTotalDestinations()
	// {
	// 	$sql = "SELECT * FROM destinations WHERE status = '1'";
	// 	$query = $this->db->query($sql);
	// 	return $query->num_rows();
	// }

   public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('truckgroup', $data);
			return ($insert == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('truckgroup', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('truckgroup');
			return ($delete == true) ? true : false;
		}
	}
}